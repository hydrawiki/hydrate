<?php declare(strict_types=1);

namespace Tests\Unit\Hydrate;

use Hydrawiki\Hydrate\Hydrator;
use InvalidArgumentException;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

class HydratorTest extends TestCase
{
    /**
     * SetUp the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fileSystem = $this->createFilesystem();
        $this->extensionSource = $this->fileSystem->url() . '/vendor';
        $this->registryMock = $this->getMockBuilder('ExtensionRegistry')
            ->setMethods(['queue'])
            ->getMock();

        $this->hydrator = new Hydrator($this->registryMock, $this->extensionSource);
    }

    /**
     * Tests that all packages in a manifest are added.
     *
     * @return void
     */
    public function testPackagesAreAddedFromManifest(): void
    {
        $this->hydrator->loadManifest($this->fileSystem->url() . '/manifest.json');

        $this->assertCount(5, $this->hydrator->getPackages());
    }

    /**
     * Tests that packages are added from an array.
     *
     * @return void
     */
    public function testPackagesAreAddedFromArray(): void
    {
        $this->hydrator->addPackages('extension', [
            'vendor-a/first-extension' => [],
            'vendor-b/second-extension' => [],
        ]);

        $this->hydrator->addPackages('skin', [
            'vendor-a/first-skin' => [],
        ]);

        $this->assertCount(3, $this->hydrator->getPackages());
    }

    /**
     * Tests that a single package is added.
     *
     * @return void
     */
    public function testPackageIsAdded(): void
    {
        $this->hydrator->addPackage('extension', 'vendor-a/first-extension');
        $this->hydrator->addPackage('skin', 'vendor-a/first-skin');

        $this->assertCount(2, $this->hydrator->getPackages());
    }

    /**
     * Tests that a Package is created with properties.
     *
     * @return void
     */
    public function testPackageIsCreatedWithProperties(): void
    {
        $package = $this->hydrator->addPackage('extension', 'vendor-a/first-extension', [
            'enabled' => false,
            'required' => true,
            'settings' => 'settings/package.php',
        ]);

        $this->assertFalse($package->isEnabled());
        $this->assertTrue($package->isRequired());
        $this->assertEquals('settings/package.php', $package->settingsPath());
    }

    /**
     * Tests that when a package is added with an unsupported type that an
     * Exception is thrown.
     *
     * @return void
     */
    public function testPackageOfInvalidTypeThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->hydrator->addPackage('invalid', 'vendor/invalid');
    }

    /**
     * Tests that enabled packages are added to the Extension Registry queue
     * when hydrate is called.
     *
     * @return void
     */
    public function testEnabledPackagesAreAddedToQueue(): void
    {
        $this->registryMock->expects($this->exactly(4))
            ->method('queue')
            ->withConsecutive(
                [$this->fileSystem->url() . '/vendor/vendor-a/first-extension/extension.json'],
                [$this->fileSystem->url() . '/vendor/vendor-b/second-extension/extension.json'],
                [$this->fileSystem->url() . '/vendor/vendor-b/third-extension/extension.json'],
                [$this->fileSystem->url() . '/vendor/vendor-a/first-skin/skin.json']
            );

        $this->hydrator->loadManifest($this->fileSystem->url() . '/manifest.json');
        $this->hydrator->hydrate();
    }

    /**
     * Tests that settings files are provided for each enabled package with a
     * settings file provided.
     *
     * @return void
     */
    public function testEnabledPackageSettingsAreProvided(): void
    {
        $this->hydrator->addPackages('extension', [
            'vendor-a/first-extension' => [
                'settings' => 'settings/first-extension.php',
            ],
            'vendor-b/second-extension' => [
                'settings' => 'settings/second-extension.php',
            ],
            'vendor-b/third-extension' => [
                'enabled' => false,
                'settings' => 'settings/third-extension.php',
            ],
        ]);

        $this->hydrator->hydrate();
        $this->assertCount(2, $this->hydrator->getSettingsFiles());
    }

    /**
     * Creates a virtual filesystem containing a package manifest and vendor
     * directory with each package and the expected config file.
     *
     * @return \org\bovigo\vfs\vfsStreamDirectory
     */
    protected function createFilesystem(): vfsStreamDirectory
    {
        return vfsStream::setup('root', null, [
            'manifest.json' => json_encode([
                'extensions' => [
                    'vendor-a/first-extension' => '{}',
                    'vendor-b/second-extension' => '{}',
                    'vendor-b/third-extension' => '{}',
                ],
                'skins' => [
                    'vendor-a/first-skin' => '{}',
                    'vendor-b/disabled-skin' => '{"enabled": false}',
                ],
            ]),
            'vendor' => [
                'vendor-a' => [
                    'first-extension' => [
                        'extension.json' => '{}',
                    ],
                    'first-skin' => [
                        'skin.json' => '{}',
                    ]
                ],
                'vendor-b' => [
                    'second-extension' => [
                        'extension.json' => '{}',
                    ],
                    'third-extension' => [
                        'extension.json' => '{}',
                    ],
                    'disabled-skin' => [
                        'skin.json' => '{}',
                    ]
                ],
            ],
        ]);
    }
}
