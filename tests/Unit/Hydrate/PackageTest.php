<?php declare(strict_types=1);

namespace Tests\Unit\Hydrate;

use Hydrawiki\Hydrate\Package;
use InvalidArgumentException;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class PackageTest extends TestCase
{
    /**
     * SetUp the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fileSystem = vfsStream::setup('root', null, [
            'vendor' => [
                'extension' => [
                    'extension.json' => '{}',
                ],
                'skin' => [
                    'skin.json' => '{}',
                ],
            ],
        ]);
    }

    /**
     * Tests that properties of the Package are set from an array.
     *
     * @return void
     */
    public function testPropertiesAreSet(): void
    {
        $package = new Package($this->fileSystem->url() . '/vendor/extension', 'extension', [
            'required' => true,
            'enabled' => false,
            'settings' => 'settings/extension.php',
        ]);

        $this->assertTrue($package->isRequired());
        $this->assertFalse($package->isEnabled());
        $this->assertEquals('settings/extension.php', $package->settingsPath());
    }

    /**
     * Tests that the config path generated for the package meets the MediaWiki
     * standard, which is required to be able to register through the
     * ExtensionRegistry.
     *
     * @return void
     */
    public function testConfigPathMeetsMediaWikiStandard(): void
    {
        $extension = new Package(
            $this->fileSystem->url() . '/vendor/extension',
            'extension'
        );

        $skin = new Package(
            $this->fileSystem->url() . '/vendor/skin',
            'skin'
        );

        $this->assertEquals(
            $this->fileSystem->url() . '/vendor/extension/extension.json',
            $extension->configPath()
        );

        $this->assertEquals(
            $this->fileSystem->url() . '/vendor/skin/skin.json',
            $skin->configPath()
        );
    }

    /**
     * Tests that when a package does not have the required config file that an
     * exception is thrown.
     *
     * @return void
     */
    public function testPackageCannotBeCreatedWithoutConfig(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $package = new Package('invalid/path', 'extension');
    }
}
