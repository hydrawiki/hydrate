<?php declare(strict_types=1);

namespace Hydrawiki\Hydrate;

use ExtensionRegistry;
use InvalidArgumentException;

class Hydrator
{
    /**
     * Package types and their configuration file name.
     *
     * @var array
     */
    const PACKAGE_TYPES = [
        'extensions' => 'extension',
        'skins' => 'skin',
    ];

    /**
     * MediaWiki Extension Registry.
     *
     * @var \ExtensionRegistry
     */
    protected $registry;

    /**
     * Path to Composer directory containing the packages.
     *
     * @var string
     */
    protected $path;

    /**
     * Packages to be hydrated.
     *
     * @var array
     */
    protected $packages = [];

    /**
     * Create a new Hydrator.
     *
     * @param \ExtensionRegistry $registry
     * @param string $path
     *
     * @return void
     */
    public function __construct(ExtensionRegistry $registry, string $path = 'vendor')
    {
        $this->registry = $registry;
        $this->path = $path;
    }

    /**
     * Load packages defined in a manifest file containing packages grouped by
     * type.
     *
     * @param string $manifest
     *
     * @return void
     */
    public function loadManifest(string $manifest): void
    {
        $packages = json_decode(file_get_contents($manifest), true);

        foreach (self::PACKAGE_TYPES as $group => $type) {
            if (array_key_exists($group, $packages)) {
                $this->addPackages($type, $packages[$group]);
            }
        }
    }

    /**
     * Add a group of packages.
     *
     * @param string $type
     * @param array $packages
     *
     * @return void
     */
    public function addPackages(string $type, array $packages): void
    {
        foreach ($packages as $package => $properties) {
            $properties = is_array($properties) ? $properties : json_decode($properties, true);
            $this->addPackage($type, $package, $properties);
        }
    }

    /**
     * Add a single package with optional properties.
     *
     * @param string $type
     * @param string $package
     * @param array $properties
     *
     * @return \Hydrawiki\Hydrate\Package
     */
    public function addPackage(string $type, string $package, array $properties = []): Package
    {
        if (! in_array($type, self::PACKAGE_TYPES)) {
            throw new InvalidArgumentException("Package type {$type} not supported.");
        }

        return $this->packages[] = new Package("{$this->path}/{$package}", $type, $properties);
    }

    /**
     * Hydrate MediaWiki with packages by queueing them in the ExtensionRegistry.
     *
     * @return void
     */
    public function hydrate(): void
    {
        foreach ($this->packages as $package) {
            if ($package->isEnabled()) {
                $this->registry->queue($package->configPath());
            }
        }
    }

    /**
     * Get all added packages.
     *
     * @return array
     */
    public function getPackages(): array
    {
        return $this->packages;
    }

    /**
     * Get available settings files for enabled packages.
     *
     * @return array
     */
    public function getSettingsFiles(): array
    {
        $settings = [];

        foreach ($this->packages as $package) {
            if ($package->isEnabled() && $package->settingsPath()) {
                $settings[$package->name] = $package->settingsPath();
            }
        }

        return $settings;
    }
}
