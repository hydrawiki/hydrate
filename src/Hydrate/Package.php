<?php declare(strict_types=1);

namespace Hydrawiki\Hydrate;

use InvalidArgumentException;

class Package
{
    /**
     * Name of the package (e.g: vendor/extension).
     *
     * @var string
     */
    public $name;

    /**
     * Type of package.
     *
     * @var string
     */
    public $type;

    /**
     * Is the package enabled?
     *
     * @var boolean
     */
    public $enabled = true;

    /**
     * Is the package required by the MediaWiki install?
     *
     * @var boolean
     */
    public $required = false;

    /**
     * Path to the package's settings for this MediaWiki install.
     *
     * @var string
     */
    public $settings = null;

    /**
     * Create a new Package.
     *
     * @param string $name
     * @param string $type
     * @param array $properties
     *
     * @return void
     */
    public function __construct(string $name, string $type, array $properties = [])
    {
        $this->name = $name;
        $this->type = $type;

        $this->setProperties($properties, [
            'enabled',
            'required',
            'settings'
        ]);

        if (! $this->isAvailable()) {
            throw new InvalidArgumentException("Required config {$this->configPath()} not found.");
        }
    }

    /**
     * Is the package required by the MediaWiki install?
     *
     * @return boolean
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * Is the package enabled?
     *
     * @return boolean
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Path to the local settings file for the package if one has been set.
     *
     * @return string|null
     */
    public function settingsPath(): ?string
    {
        return $this->settings;
    }

    /**
     * Path to the config file for the package based on MediaWiki convention.
     *
     * @return string
     */
    public function configPath(): string
    {
        return "{$this->name}/{$this->type}.json";
    }

    /**
     * Is the package available to register with the ExtensionRegistry?
     *
     * @return boolean
     */
    public function isAvailable(): bool
    {
        return file_exists($this->configPath());
    }

    /**
     * Set the allowed properties from an array.
     *
     * @param array $values
     * @param array $properties
     *
     * @return void
     */
    protected function setProperties(array $values, array $properties): void
    {
        foreach ($properties as $property) {
            if (array_key_exists($property, $values)) {
                $this->{$property} = $values[$property];
            }
        }
    }
}
