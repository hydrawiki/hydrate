# Hydrate

Hydrate MediaWiki with Extensions and Skins installed through Composer.

## How To

First, install Hydrate via Composer:

```bash
$ composer require hydrawiki/hydrate
```

After installing Hydrate, remove all references to `wfLoadExtension` and
`wfLoadSkin` from `LocalSettings.php` then initialise Hydrator with the
MediaWiki ExtensionRegistry instance, `loadManifest` to add all packages and
`hydrate` to queue all packages
e.g:

```php
$hydrator = new Hydrawiki\Hydrate\Hydrator(ExtensionRegistry::getInstance());
$hydrator->loadManifest(__DIR__ . '/hydrate.json');
$hydrator->hydrate();
```

Your MediaWiki install will now register any Composer-managed MediaWiki
Extension or Skin in your `hydrate.json` Package manifest.

```json
{
  "extensions": {
    "vendor-a/extension-1": {},
    "vendor-b/extension-2": {},
    "vendor-b/extension-3": {}
  },
  "skins": {
    "vendor-a/skin-1": {},
    "vendor-b/skin-2": {},
    "vendor-c/skin-3": {}
  }
}
```

### Installing Packages

Many MediaWiki packages implement the MediaWiki
[Composer Installer](https://github.com/composer/installers) which automatically
moves Extensions and Skins out of `vendor` and into `extensions` and `skins`
which is not desirable behaviour in this situation. For this reason, [disable the
MediaWiki installer](https://github.com/composer/installers#disabling-installers)
before installing any packages.

```json
{
  "extra": {
    "installer-disable": [
      "mediawiki"
    ]
  }
}
```

### Package Settings

Each package in the manifest may specify a settings file which is available to
include when the package is enabled, for example:

`hydrate.json`

```json
{
  "extensions": {
    "vendor/extension": {
      "settings": "settings/extension.php"
    }
  }
}
```

`LocalSettings.php`

```php
foreach ($hydrator->getSettingsFiles() as $setting) {
    require_once $setting;
}
```

### All Properties

| Property | Type          | Default | Description |
|----------|---------------|---------|-------------|
| Enabled  | `Boolean`     | `true`  | Enabled packages are added to extension registry |
| Required | `Boolean`     | `false` | Required packages cannot be disabled by users |
| Settings | `String`      | `null`  | Path to settings file for package |

## Why

Package managers enable easy management of dependencies, unfortunately the
MediaWiki ecosystem has not fully embraced package management, many extensions
are installed by manually adding third-party files to the codebase. Hydrate aims
to bridge the gap between modern package management and MediaWiki with low
friction by harnessing Composer and the MediaWiki Extension Registry.

## Caveats

Hydrate requires packages (Extensions, Skins) use the MediaWiki >=1.2.5
configuration files (`extension.json` and `skin.json`). Any package that does
not follow this standard is not compatible with Hydrate out of the box, however
it is possible to add these configurations to packages with some work.

Hydrate requires packages are available via Composer. Making a package available
via Composer is simple, for any company that maintains their own extension
repositories it should take no more than a couple of minutes per extension.

* [Submit a package to Packagist](https://packagist.org/about#how-to-submit-packages)
* [Add a package to a private Packagist repository](https://getcomposer.org/doc/articles/handling-private-packages-with-satis.md)
* [Add a package from a local repository](https://getcomposer.org/doc/05-repositories.md#path)

Hydrate is an experiment in providing a more maintainable approach to
managing skin and extension dependencies, it has not been extensively used in
production MediaWiki installs yet.

## License

```
MIT License

Copyright (c) 2019 Samuel Ryan <sryan@fandom.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
